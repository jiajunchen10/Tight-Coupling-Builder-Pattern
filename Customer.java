package tightCoupling;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

import javax.swing.JOptionPane;
public class Customer {
	private String customerFirstName;
	private String customerLastName;        
	private String customerPhone;            
	private String customerStreet;
	private String customerCity;
	private String customerState;
	private String customerZipCode;          
	private LocalDate customerBirthday;      
	private LocalDate customerRegistrationDay;     
	private String customerIDCard;           
	private int customerAge;                 
	private Customer(CustomerBuilder temp){
		this.customerAge = temp.customerAge;
		this.customerBirthday = temp.customerBirthday;
		this.customerCity = temp.customerCity;
		this.customerFirstName = temp.customerFirstName;
		this.customerIDCard = temp.customerIDCard;
		this.customerLastName = temp.customerLastName;
		this.customerPhone = temp.customerPhone;
		this.customerRegistrationDay = temp.customerRegistrationDay;
		this.customerState = temp.customerState;
		this.customerStreet = temp.customerStreet;
		this.customerZipCode = temp.customerZipCode;
	}
	public int getCustomerAge(){
		return this.customerAge;
	}
	public String getCustomerName(){
		return this.customerFirstName+ " "+ this.customerLastName;
	}
	public String getCustomerAddress(){
		return this.customerStreet + " "+ this.customerCity+", "+this.customerState+", "+this.customerZipCode;
	}
	public LocalDate getCustomerBirthday(){
		return this.customerBirthday;
	}
	public LocalDate getCustomerRegistrationDate(){
		return this.customerRegistrationDay;
	}
	public String getCustomerIDCard(){
		return this.customerIDCard;
	}
	public String toString(){
		String result = "Customer Name: "+this.customerFirstName+ " "+ this.customerLastName+"\n";
		result+= "Customer Address: "+ this.customerStreet+" "+this.customerCity+", "+this.customerState
				+", "+ this.customerZipCode+"\n";
		result+="Customer Registration Date: "+this.customerRegistrationDay.getMonth()+" "+ 
				this.customerRegistrationDay.getDayOfMonth()+" "+this.customerRegistrationDay.getYear()+"\n";
		result+="Customer Age: "+ this.customerAge+"\n";
		result+="Customer Birthday: "+ this.customerBirthday.getMonthValue()+" "+ this.customerBirthday
				.getDayOfMonth()+" "+this.customerBirthday.getYear()+"\n";
		result+="Customer Phone: "+this.customerPhone+"\n";
		result+="Customer ID: "+this.customerIDCard+"\n";
		return result;
	}
	public static final class CustomerBuilder{
		private String customerFirstName;
		private String customerLastName;         //name
		private String customerPhone;            //phone
		private String customerStreet;
		private String customerCity;
		private String customerState;
		private String customerZipCode;          // address
		private LocalDate customerBirthday;      // birthday
		private LocalDate customerRegistrationDay;     // filled in the builder build
		private String customerIDCard;           // generates in the builder build
		private int customerAge;                 // calculated in the setbirthday method
		
		public CustomerBuilder setName(String first, String last){
			this.customerFirstName=first;
			this.customerLastName=last;
			return this;
		}
		public CustomerBuilder setPhone(String phone){
			long phoneNumber=0l;
			boolean keepGoing = false;
			do{
				try{
					keepGoing = false;
					phone = phone.replaceAll("-", " ").trim();
					phoneNumber = Long.parseLong(phone);
				}catch(NumberFormatException e){
					phone = JOptionPane.showInputDialog("invalid number, please enter again");
					keepGoing = true;
				}
				if(!keepGoing)   //done
				if(!keepGoing && phoneNumber<=999999999){  //done but invalid number
					keepGoing = true;     
					phone = JOptionPane.showInputDialog("invalid number, please enter again");
				}
			}while(keepGoing);
			this.customerPhone = ""+phoneNumber;
			return this;
		}
		public CustomerBuilder setAddress(String street, String city, String state, String zip){
			this.customerCity = city;
			this.customerState = state;
			this.customerStreet = street;
			boolean keepGoing = false;
			do{
				try{
					keepGoing = false;
					this.customerZipCode = "" + Long.parseLong(zip);
				}catch(NumberFormatException e){
					zip = JOptionPane.showInputDialog("invalid zipcode, please enter again");
					keepGoing = true;
				}
				if(!keepGoing)
					if(!keepGoing && Integer.parseInt(zip)>=99999)
					{
						keepGoing = true;
						zip = JOptionPane.showInputDialog("invalid zipcode, pease try again");
					}
			}while(keepGoing);
			this.customerZipCode = zip;
			return this;
		}
		public CustomerBuilder setBirthDay(LocalDate birthdate){
			LocalDate today = LocalDate.now();
			this.customerAge = today.getYear()-birthdate.getYear() -1;
			this.customerAge += (today.getMonthValue()>=birthdate.getMonthValue())? 1:0;
			this.customerBirthday = birthdate;
			return this;
		}
		public Customer build(){
			this.customerRegistrationDay = LocalDate.now();
			this.customerIDCard = ""+this.customerFirstName.charAt(0)+this.customerLastName.charAt(0)+this.customerPhone;
			if(this.customerAge!=0 && this.customerBirthday!=null && 
			   this.customerCity!=null && this.customerFirstName!=null && 
			   this.customerIDCard!=null && this.customerLastName!=null && 
			   this.customerPhone!=null && this.customerRegistrationDay!=null &&
			   this.customerState!=null && this.customerStreet!=null && 
			   this.customerZipCode!=null)
				return new Customer(this);
			else
				throw new IllegalStateException("please fill in all information to create a customer account");
		}
	}
	public static void main(String args[]){
		Customer mine = new CustomerBuilder().setName("Jiajun", "Chen").setAddress("1 Main St.",
				"Malden", "MA", "02148").setBirthDay(LocalDate.of(1990,Month.JANUARY,01)).setPhone("1111111111").build();
		System.out.println(mine.toString());
	}
}
